# FVTT-DD-Importer
Allows Importing DungeonDraft map files into FoundryVTT

**Version 0.7**: Adds offset for Cave Walls, thanks @m42e!

Manifest: `https://raw.githubusercontent.com/moo-man/FVTT-DD-Import/master/module.json`
